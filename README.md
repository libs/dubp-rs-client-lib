# dubp-rs-client-lib

This library is intended to serve as a common base for all client software using Rust, including (non-exhaustive list):

* [ğcli](https://git.duniter.org/clients/rust/gcli)
* [ğecko](https://git.duniter.org/clients/gecko)

## Async or blocking

This library is asynchronous by default.

To use it in "blocking" mode (non-asynchronous), you must disable the default features and enable the `blocking` feature:

```toml
[dependencies]
dubp-client = { git = "https://git.duniter.org/libs/dubp-rs-client-lib", branch = "master", features = ["blocking"] }
```

## Contribute

Contributions are welcome :)

If you have any questions about the code don't hesitate to ask @elois on the duniter forum: [https://forum.duniter.org](https://forum.duniter.org)

The GraphQL schema is automatically generated from the GVA source code, to update the schema, use the following commands:

    cargo update -p duniter-gva-gql
    cargo bs
