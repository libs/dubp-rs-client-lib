//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

mod account_balance;
mod current_ud;
mod idty_by_pubkey;
mod members_count;
mod simple_payment;

use crate::*;

#[maybe_async]
#[cfg_attr(feature = "async", async_trait)]
#[cfg_attr(feature = "mock", mockall::automock)]
pub trait GvaClient: inner::GvaClientInner {
    #[cfg_attr(not(feature = "mock"), inline(always))]
    async fn account_balance(
        &self,
        pubkey_or_script: &PubkeyOrScript,
        with_ud: bool,
    ) -> Result<Option<AccountBalance>, GvaClientError> {
        account_balance::account_balance(self, pubkey_or_script, with_ud).await
    }
    #[cfg_attr(not(feature = "mock"), inline(always))]
    async fn current_ud(&self) -> Result<Option<u64>, GvaClientError> {
        current_ud::current_ud(self).await
    }
    #[cfg_attr(not(feature = "mock"), inline(always))]
    async fn idty_by_pubkey(&self, pubkey: PublicKey) -> Result<Option<Idty>, GvaClientError> {
        idty_by_pubkey::idty_by_pubkey(self, pubkey).await
    }
    #[cfg_attr(not(feature = "mock"), inline(always))]
    async fn members_count(&self) -> Result<u64, GvaClientError> {
        members_count::members_count(self).await
    }
    #[cfg_attr(not(feature = "mock"), inline(always))]
    async fn simple_payment<S>(
        &self,
        amount: Amount,
        issuer: &S,
        recipient: WalletScriptV10,
        tx_comment: Option<String>,
        cash_back_pubkey: Option<PublicKey>,
    ) -> Result<PaymentResult, GvaClientError>
    where
        S: 'static + Send + Sync + Signator<PublicKey = PublicKey>,
    {
        simple_payment::simple_payment(
            self,
            amount,
            issuer,
            recipient,
            tx_comment,
            cash_back_pubkey,
        )
        .await
    }
}

// Entities

#[derive(Debug)]
pub enum PaymentResult {
    Ok,
    Errors(Vec<duniter_bca_types::rejected_tx::RejectedTxReason>),
}

// Errors

#[derive(Debug, Error)]
pub enum GvaClientError {
    #[error("{0}")]
    Custom(String),
    #[error("server response contains no data")]
    EmptyResponse,
    #[error("{0:?}")]
    Errors(Vec<GvaClientError>),
    #[error("fail to deserialize server response: {0}")]
    FailToDeserResp(bincode::Error),
    #[error("{0}")]
    Req(reqwest::Error),
    #[error("{0}")]
    Bca(BcaReqExecError),
    #[error("{0}")]
    Gql(GqlServerErrors),
    #[error("Invalid endpoint url: {0}")]
    InvalidUrl(String),
    #[error("Server send invalid bca response type: {0:?}")]
    ServerSendInvalidBcaRespTypeV0(BcaRespTypeV0),
    #[error("Server send invalid bca response type or version: {0:?}")]
    ServerSendInvalidBcaRespTypeOrVersion(BcaResp),
    #[cfg(feature = "blocking")]
    #[error("{0}")]
    ThreadPoolDisconnected(fast_threadpool::ThreadPoolDisconnected),
    #[error("unknown error")]
    UnknownError,
}

impl Default for GvaClientError {
    fn default() -> Self {
        GvaClientError::UnknownError
    }
}

#[derive(Debug)]
pub struct GqlServerErrors(pub Vec<graphql_client::Error>);
impl Display for GqlServerErrors {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s: Vec<String> = self.0.iter().map(ToString::to_string).collect();
        write!(f, "{}", s.join(",/n"))
    }
}

// Inner

#[cfg(feature = "mock")]
#[maybe_async]
#[cfg_attr(feature = "async", async_trait)]
impl inner::GvaClientInner for MockGvaClient {
    async fn send_bca_query(&self, _req: BcaReq) -> Result<BcaResp, GvaClientError> {
        unimplemented!()
    }
    async fn send_bca_batch_queries<I: IntoIterator<Item = BcaReq> + Send + Sync + 'static>(
        &self,
        _batch_iter: I,
    ) -> Result<Vec<BcaResult>, GvaClientError> {
        unimplemented!()
    }
}

pub(crate) mod inner {
    use crate::*;

    #[cfg(feature = "async")]
    #[async_trait]
    #[cfg_attr(feature = "mock", mockall::automock)]
    pub trait GvaClientInner: Send + Sized + Sync {
        async fn send_bca_query(&self, req: BcaReq) -> Result<BcaResp, GvaClientError>;
        async fn send_bca_batch_queries<S: Stream<Item = BcaReq> + Send + Sync + 'static>(
            &self,
            batch_stream: S,
        ) -> Result<Vec<BcaResult>, GvaClientError>;
    }

    #[cfg(feature = "blocking")]
    #[cfg_attr(feature = "mock", mockall::automock)]
    pub trait GvaClientInner: Send + Sized + Sync {
        fn send_bca_query(&self, req: BcaReq) -> Result<BcaResp, GvaClientError>;
        fn send_bca_batch_queries<I: IntoIterator<Item = BcaReq> + Send + Sync + 'static>(
            &self,
            batch_iter: I,
        ) -> Result<Vec<BcaResult>, GvaClientError>;
    }
}
