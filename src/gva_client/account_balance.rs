//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use super::*;

#[maybe_async]
pub(super) async fn account_balance<C: GvaClient>(
    gva_client: &C,
    pubkey_or_script: &PubkeyOrScript,
    with_ud: bool,
) -> Result<Option<AccountBalance>, GvaClientError> {
    let mut arr = arrayvec::ArrayVec::new();
    arr.push(pubkey_or_script.0.clone());
    let req = BcaReq::V0(BcaReqV0 {
        req_id: 0,
        req_type: BcaReqTypeV0::BalancesOfScripts(arr),
    });

    if with_ud {
        let req2 = BcaReq::V0(BcaReqV0 {
            req_id: 1,
            req_type: BcaReqTypeV0::CurrentUd,
        });
        let batch = vec![req, req2];
        cfg_if::cfg_if! {
            if #[cfg(feature = "async")] {
                let batch = futures::stream::iter(batch.into_iter());
            }
        }
        let mut resps: Vec<BcaRespV0> = gva_client
            .send_bca_batch_queries(batch)
            .await?
            .into_iter()
            .map_ok(|resp| match resp {
                BcaResp::V0(resp_v0) => Ok(resp_v0),
                o => Err(GvaClientError::ServerSendInvalidBcaRespTypeOrVersion(o)),
            })
            .collect::<Result<Result<Vec<_>, _>, _>>()
            .map_err(GvaClientError::Bca)??;
        resps.sort_unstable_by(|r1, r2| r2.req_id.cmp(&r1.req_id));

        let current_ud = match &resps[0].resp_type {
            BcaRespTypeV0::CurrentUd(current_ud) => current_ud,
            BcaRespTypeV0::Error(e) => return Err(GvaClientError::Custom(e.to_owned())),
            o => return Err(GvaClientError::ServerSendInvalidBcaRespTypeV0(o.to_owned())),
        };

        match &resps[1].resp_type {
            BcaRespTypeV0::Balances(arr) => {
                if let Some(Some(balance)) = arr.get(0) {
                    Ok(Some(AccountBalance {
                        amount: *balance,
                        ud_amount_opt: Some(*current_ud),
                    }))
                } else {
                    Ok(None)
                }
            }
            BcaRespTypeV0::Error(e) => Err(GvaClientError::Custom(e.to_owned())),
            o => Err(GvaClientError::ServerSendInvalidBcaRespTypeV0(o.to_owned())),
        }
    } else {
        let resp = gva_client.send_bca_query(req).await?;
        bca_resp_v0!(resp, BcaRespTypeV0::Balances(arr), {
            if let Some(Some(balance)) = arr.get(0) {
                Ok(Some(AccountBalance {
                    amount: *balance,
                    ud_amount_opt: None,
                }))
            } else {
                Ok(None)
            }
        })
    }
}
