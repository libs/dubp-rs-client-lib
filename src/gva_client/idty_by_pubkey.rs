//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the ter is_member: (), pubkey: (), username: ()ms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use super::*;

#[maybe_async]
pub(super) async fn idty_by_pubkey<C: GvaClient>(
    gva_client: &C,
    pubkey: PublicKey,
) -> Result<Option<Idty>, GvaClientError> {
    let mut arr = arrayvec::ArrayVec::new();
    arr.push(pubkey);
    let req = BcaReq::V0(BcaReqV0 {
        req_id: 0,
        req_type: BcaReqTypeV0::Identities(arr),
    });

    let resp = gva_client.send_bca_query(req).await?;
    bca_resp_v0!(resp, BcaRespTypeV0::Identities(arr), {
        if let Some(Some(idty)) = arr.get(0) {
            Ok(Some(Idty {
                is_member: idty.is_member,
                pubkey,
                username: idty.username.clone(),
            }))
        } else {
            Ok(None)
        }
    })
}
