//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use super::*;
use dubp::{
    common::prelude::*,
    documents::transaction::{
        TransactionDocumentTrait as _, UnsignedTransactionDocumentTrait as _,
    },
    documents::SignedOrUnsignedDocument,
};
use duniter_bca_types::prepare_payment::{PrepareSimplePayment, PrepareSimplePaymentResp};

#[maybe_async]
pub(super) async fn simple_payment<C, S>(
    gva_client: &C,
    amount: Amount,
    issuer_signator: &S,
    recipient: WalletScriptV10,
    tx_comment: Option<String>,
    cash_back_pubkey: Option<PublicKey>,
) -> Result<PaymentResult, GvaClientError>
where
    C: GvaClient,
    S: Signator<PublicKey = PublicKey>,
{
    let bca_resp = gva_client
        .send_bca_query(BcaReq::V0(BcaReqV0 {
            req_id: 0,
            req_type: BcaReqTypeV0::PrepareSimplePayment(PrepareSimplePayment {
                issuer: issuer_signator.public_key(),
                amount,
            }),
        }))
        .await?;

    match bca_resp {
        BcaResp::V0(BcaRespV0 {
            req_id: _,
            resp_type,
        }) => match resp_type {
            BcaRespTypeV0::PrepareSimplePayment(PrepareSimplePaymentResp {
                current_block_number,
                current_block_hash,
                current_ud,
                inputs,
                inputs_sum,
            }) => {
                let amount = amount.to_cents(current_ud);
                let unsigned_txs =
                    dubp::documents::transaction::TransactionDocumentV10::generate_simple_txs(
                        Blockstamp {
                            number: BlockNumber(current_block_number),
                            hash: BlockHash(current_block_hash),
                        },
                        "g1".to_owned(),
                        (inputs, inputs_sum),
                        issuer_signator.public_key(),
                        recipient,
                        (amount, tx_comment.unwrap_or_default()),
                        cash_back_pubkey,
                    );

                let bca_resp = gva_client
                    .send_bca_query(BcaReq::V0(BcaReqV0 {
                        req_id: 0,
                        req_type: BcaReqTypeV0::SendTxs(
                            unsigned_txs
                                .into_iter()
                                .map(|unsigned_tx| {
                                    if let Ok(SignedOrUnsignedDocument::Signed(tx)) =
                                        unsigned_tx.sign(issuer_signator)
                                    {
                                        tx
                                    } else {
                                        panic!("dev error: client generate an invalid tx")
                                    }
                                })
                                .collect(),
                        ),
                    }))
                    .await?;

                match bca_resp {
                    BcaResp::V0(BcaRespV0 {
                        req_id: _,
                        resp_type,
                    }) => match resp_type {
                        BcaRespTypeV0::RejectedTxs(rejected_txs) => {
                            if rejected_txs.is_empty() {
                                Ok(PaymentResult::Ok)
                            } else {
                                Ok(PaymentResult::Errors(
                                    rejected_txs
                                        .into_iter()
                                        .map(|rejected_tx| rejected_tx.reason)
                                        .collect(),
                                ))
                            }
                        }
                        BcaRespTypeV0::Error(e) => Err(GvaClientError::Custom(e)),
                        o => Err(GvaClientError::ServerSendInvalidBcaRespTypeV0(o)),
                    },
                    o => Err(GvaClientError::ServerSendInvalidBcaRespTypeOrVersion(o)),
                }
            }
            BcaRespTypeV0::Error(e) => Err(GvaClientError::Custom(e)),
            o => Err(GvaClientError::ServerSendInvalidBcaRespTypeV0(o)),
        },
        o => Err(GvaClientError::ServerSendInvalidBcaRespTypeOrVersion(o)),
    }
}
