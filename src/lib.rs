//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#![deny(
    clippy::expect_used,
    clippy::unwrap_used,
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces
)]

static_assertions::assert_cfg!(
    not(all(feature = "async", feature = "blocking")),
    "The async and blocking features are not compatible."
);
static_assertions::assert_cfg!(
    any(feature = "async", feature = "blocking"),
    "You must enable the async or blocking feature."
);

mod gva_client;
#[cfg(feature = "blocking")]
mod multi_gva_client;
mod naive_gva_client;
mod raw_requestor;
#[cfg(feature = "blocking")]
mod utils;

// Re-export crates
pub use dubp::common;
pub use dubp::crypto;
pub use dubp::documents;
pub use dubp::documents_parser;
pub use dubp::wallet;

// Public imports
#[cfg(feature = "mock")]
pub use crate::gva_client::MockGvaClient;
pub use crate::gva_client::{GqlServerErrors, GvaClient, GvaClientError, PaymentResult};
#[cfg(feature = "blocking")]
pub use crate::multi_gva_client::{MultiGvaClient, MultiGvaClientBuildError};
pub use crate::naive_gva_client::{NaiveGvaClient, NaiveGvaClientBuildError};
pub use dubp::crypto::keys::{KeyPair, Signator};
pub use duniter_bca_types::amount::Amount;

// Crate inner imports
#[cfg(test)]
pub(crate) use crate::raw_requestor::MockRawRequestor;
#[cfg(test)]
pub(crate) type RawRequestor = std::sync::Arc<MockRawRequestor>;
#[cfg(not(test))]
pub(crate) use crate::raw_requestor::RawRequestor;
#[cfg(feature = "async")]
pub(crate) use async_trait::async_trait;
pub(crate) use dubp::crypto::keys::{ed25519::PublicKey, PublicKey as _};
pub(crate) use dubp::documents_parser::dubp_wallet::prelude::*;
pub(crate) use dubp::documents_parser::prelude::*;
pub(crate) use duniter_bca_types::{BcaReq, BcaReqExecError, BcaResp};
pub(crate) use duniter_bca_types::{BcaReqTypeV0, BcaReqV0, BcaRespTypeV0, BcaRespV0};
#[cfg(feature = "async")]
pub(crate) use futures::Stream;
pub(crate) use maybe_async::maybe_async;
#[cfg(feature = "blocking")]
pub(crate) use reqwest::blocking::Client;
#[cfg(not(feature = "blocking"))]
pub(crate) use reqwest::Client;
pub(crate) use reqwest::Url;
pub(crate) use resiter::map::Map;
pub(crate) use std::{fmt::Display, str::FromStr};
pub(crate) use thiserror::Error;

pub(crate) type BcaResult = Result<BcaResp, BcaReqExecError>;

#[derive(Clone, Copy, Debug)]
pub struct AccountBalance {
    pub amount: SourceAmount,
    pub ud_amount_opt: Option<SourceAmount>,
}

#[derive(Clone, Debug)]
pub struct Idty {
    pub is_member: bool,
    pub pubkey: PublicKey,
    pub username: String,
}

#[derive(Debug)]
pub struct PubkeyOrScript(pub(crate) WalletScriptV10);

impl FromStr for PubkeyOrScript {
    type Err = TextParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Ok(pubkey) = PublicKey::from_base58(s) {
            Ok(Self(WalletScriptV10::single_sig(pubkey)))
        } else {
            Ok(Self(dubp::documents_parser::wallet_script_from_str(s)?))
        }
    }
}

impl ToString for PubkeyOrScript {
    fn to_string(&self) -> String {
        self.0.to_string()
    }
}

#[cfg(not(test))]
pub(crate) fn raw_requestor() -> RawRequestor {
    RawRequestor
}
#[cfg(test)]
pub(crate) fn raw_requestor() -> RawRequestor {
    std::sync::Arc::new(MockRawRequestor::new(|_, _| {
        Ok(BcaResp::V0(BcaRespV0 {
            req_id: 0,
            resp_type: BcaRespTypeV0::Pong,
        }))
    }))
}

#[macro_export]
macro_rules! bca_resp_v0 {
    ($resp: ident, $pattern: pat, $expr: expr) => {
        match $resp {
            BcaResp::V0(resp_v0) => match resp_v0.resp_type {
                $pattern => $expr,
                BcaRespTypeV0::Error(e) => Err(GvaClientError::Custom(e)),
                o => Err(GvaClientError::ServerSendInvalidBcaRespTypeV0(o)),
            },
            o => Err(GvaClientError::ServerSendInvalidBcaRespTypeOrVersion(o)),
        }
    };
}
