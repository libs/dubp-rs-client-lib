//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

mod config;
mod wave;

use crate::gva_client::inner::GvaClientInner;
use crate::*;
use config::MultiGvaClientConfig;
use fast_threadpool::{ThreadPool, ThreadPoolConfig, ThreadPoolSyncHandler};

#[derive(Debug)]
pub struct MultiGvaClient {
    config: MultiGvaClientConfig,
    endpoints: Vec<Url>,
    raw_requestor: RawRequestor,
    threadpool: ThreadPoolSyncHandler<Client>,
}

#[derive(Debug, Error, PartialEq)]
pub enum MultiGvaClientBuildError {
    #[error("Invalid endpoint url: {0}")]
    InvalidUrl(String),
}

impl MultiGvaClient {
    #[inline(always)]
    pub fn new(endpoints: Vec<String>) -> Result<Self, MultiGvaClientBuildError> {
        Self::new_inner(MultiGvaClientConfig::default(), endpoints, raw_requestor())
    }
    pub fn new_with_config(
        config: MultiGvaClientConfig,
        endpoints: Vec<String>,
    ) -> Result<Self, MultiGvaClientBuildError> {
        Self::new_inner(config, endpoints, raw_requestor())
    }
    fn new_inner(
        config: MultiGvaClientConfig,
        endpoints: Vec<String>,
        raw_requestor: RawRequestor,
    ) -> Result<Self, MultiGvaClientBuildError> {
        Ok(Self {
            config,
            endpoints: endpoints
                .into_iter()
                .map(|ep| Url::parse(&ep))
                .collect::<Result<Vec<_>, _>>()
                .map_err(|e| MultiGvaClientBuildError::InvalidUrl(e.to_string()))?,
            raw_requestor,
            threadpool: ThreadPool::start(ThreadPoolConfig::default(), Client::new())
                .into_sync_handler(),
        })
    }
}

impl GvaClientInner for MultiGvaClient {
    fn send_bca_query(
        &self,
        req: duniter_bca_types::BcaReq,
    ) -> Result<duniter_bca_types::BcaResp, crate::GvaClientError> {
        let parallel = self.config.parallel.get();

        let mut iter =
            crate::utils::choose_multiple(self.endpoints.as_slice(), self.endpoints.len()).cloned();
        let mut errors = vec![];

        let mut waves_count = self.endpoints.len() / parallel;
        if waves_count % parallel > 0 {
            waves_count += 1;
        }
        for _ in 0..waves_count {
            match wave::send_wave(
                &mut iter,
                parallel,
                self.raw_requestor.clone(),
                req.clone(),
                &self.threadpool,
            ) {
                Ok(resp) => return Ok(resp),
                Err(errors_) => errors.push(errors_),
            }
        }
        Err(crate::GvaClientError::Errors(errors))
    }

    fn send_bca_batch_queries<I: IntoIterator<Item = BcaReq> + Send + Sync + 'static>(
        &self,
        _batch_iter: I,
    ) -> Result<Vec<crate::BcaResult>, crate::GvaClientError> {
        todo!()
    }
}

#[allow(clippy::expect_used)]
#[cfg(test)]
mod tests {
    use std::num::NonZeroUsize;

    use super::*;
    use assert_matches::assert_matches;
    use duniter_bca_types::BcaReqV0;

    #[test]
    fn test_init_with_more_than_one_endpoint() -> Result<(), MultiGvaClientBuildError> {
        let endpoints = vec!["https://toto.org".to_owned(), "http://plop.fr".to_owned()];
        MultiGvaClient::new(endpoints)?;

        Ok(())
    }

    #[test]
    fn test_init_with_invalid_url() {
        let endpoints = vec!["invalid url".to_owned()];
        assert_matches!(
            MultiGvaClient::new(endpoints),
            Err(MultiGvaClientBuildError::InvalidUrl(_))
        );
    }

    fn create_gva_client(
        config: MultiGvaClientConfig,
        mock_opt: Option<MockRawRequestor>,
    ) -> MultiGvaClient {
        let endpoints = vec!["https://toto.org", "http://plop.fr", "http://tata.com"]
            .iter()
            .map(ToString::to_string)
            .collect();
        if let Some(mock) = mock_opt {
            MultiGvaClient::new_inner(config, endpoints, std::sync::Arc::new(mock))
                .expect("invalid endpoint")
        } else {
            MultiGvaClient::new_with_config(config, endpoints).expect("invalid endpoint")
        }
    }

    #[test]
    fn test_basic_request() {
        let req = BcaReq::V0(BcaReqV0 {
            req_id: 0,
            req_type: BcaReqTypeV0::Ping,
        });
        let expected_resp = BcaResp::V0(BcaRespV0 {
            req_id: 0,
            resp_type: BcaRespTypeV0::Pong,
        });

        let req_clone = req.clone();
        let expected_resp_clone = expected_resp.clone();
        let mock = MockRawRequestor::new(move |req_, _| {
            if req_ == req_clone {
                Ok(expected_resp_clone.clone())
            } else {
                Err(GvaClientError::UnknownError)
            }
        });

        let gva_client = create_gva_client(MultiGvaClientConfig::default(), Some(mock));
        let resp = gva_client
            .send_bca_query(req)
            .expect("send_bca_query() return error");
        assert_eq!(resp, expected_resp)
    }

    #[test]
    fn test_basic_request_with_first_node_down() {
        let req = BcaReq::V0(BcaReqV0 {
            req_id: 0,
            req_type: BcaReqTypeV0::Ping,
        });
        let expected_resp = BcaResp::V0(BcaRespV0 {
            req_id: 0,
            resp_type: BcaRespTypeV0::Pong,
        });

        let expected_resp_clone = expected_resp.clone();
        let mock = MockRawRequestor::new(move |_, url| {
            if url.host_str() == Some("plop.fr") {
                Err(GvaClientError::UnknownError)
            } else {
                Ok(expected_resp_clone.clone())
            }
        });

        let gva_client = create_gva_client(MultiGvaClientConfig::default(), Some(mock));
        let resp = gva_client
            .send_bca_query(req)
            .expect("send_bca_query() return error");
        assert_eq!(resp, expected_resp)
    }

    #[test]
    fn test_basic_request_with_all_nodes_down() {
        let req = BcaReq::V0(BcaReqV0 {
            req_id: 0,
            req_type: BcaReqTypeV0::Ping,
        });

        let mock = MockRawRequestor::new(move |_, _| Err(GvaClientError::UnknownError));
        let gva_client = create_gva_client(MultiGvaClientConfig::default(), Some(mock));
        let resp = gva_client.send_bca_query(req);
        assert_matches!(resp, Err(GvaClientError::Errors(_)))
    }

    #[test]
    fn test_basic_request_in_parallel() {
        let req = BcaReq::V0(BcaReqV0 {
            req_id: 0,
            req_type: BcaReqTypeV0::Ping,
        });
        let expected_resp = BcaResp::V0(BcaRespV0 {
            req_id: 0,
            resp_type: BcaRespTypeV0::Pong,
        });

        let expected_resp_clone = expected_resp.clone();
        let mock = MockRawRequestor::new(move |_, url| {
            if url.host_str() == Some("toto.org") {
                std::thread::sleep(std::time::Duration::from_secs(1));
                Ok(expected_resp_clone.clone())
            } else {
                Ok(expected_resp_clone.clone())
            }
        });

        let gva_client = create_gva_client(
            MultiGvaClientConfig {
                parallel: NonZeroUsize::new(2).expect("unreachable"),
                ..Default::default()
            },
            Some(mock),
        );
        let resp = gva_client
            .send_bca_query(req)
            .expect("send_bca_query() return error");
        assert_eq!(resp, expected_resp)
    }

    #[test]
    fn test_basic_request_in_parallel_with_all_fail_on_first_wave() {
        let req = BcaReq::V0(BcaReqV0 {
            req_id: 0,
            req_type: BcaReqTypeV0::Ping,
        });
        let expected_resp = BcaResp::V0(BcaRespV0 {
            req_id: 0,
            resp_type: BcaRespTypeV0::Pong,
        });

        let expected_resp_clone = expected_resp.clone();
        let mock = MockRawRequestor::new(move |_, url| {
            //"https://toto.org", "http://plop.fr", "http://tata.com"
            if url.host_str() == Some("tata.com") {
                Ok(expected_resp_clone.clone())
            } else {
                Err(GvaClientError::UnknownError)
            }
        });

        let gva_client = create_gva_client(
            MultiGvaClientConfig {
                parallel: NonZeroUsize::new(2).expect("unreachable"),
                ..Default::default()
            },
            Some(mock),
        );
        let resp = gva_client
            .send_bca_query(req)
            .expect("send_bca_query() return error");
        assert_eq!(resp, expected_resp)
    }
}
