//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::num::NonZeroUsize;

#[allow(unsafe_code)]
const ONE: NonZeroUsize = unsafe { NonZeroUsize::new_unchecked(1) };

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct MultiGvaClientConfig {
    pub parallel: NonZeroUsize,
    pub threshold: NonZeroUsize,
}

impl Default for MultiGvaClientConfig {
    fn default() -> Self {
        Self {
            parallel: ONE,
            threshold: ONE,
        }
    }
}
