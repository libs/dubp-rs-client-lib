//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use fast_threadpool::ThreadPoolSyncHandler;

use crate::*;

pub(super) fn send_wave<I: Iterator<Item = Url>>(
    endpoints: &mut I,
    parallel: usize,
    raw_requestor: RawRequestor,
    req: BcaReq,
    threadpool: &ThreadPoolSyncHandler<Client>,
) -> Result<duniter_bca_types::BcaResp, crate::GvaClientError> {
    let mut errors = vec![];

    let (s, r) = flume::bounded(parallel);

    let mut wave_len = 0;
    for _ in 0..parallel {
        if let Some(ep) = endpoints.next() {
            let raw_requestor = raw_requestor.clone();
            let req_clone = req.clone();
            threadpool
                .launch_channel(
                    move |client| raw_requestor.send_bca_query(req_clone, &client, ep),
                    s.clone(),
                )
                .map_err(GvaClientError::ThreadPoolDisconnected)?;
        }
        wave_len += 1;
    }

    for _ in 0..wave_len {
        if let Ok(resp_res) = r.recv() {
            match resp_res {
                Ok(resp) => return Ok(resp),
                Err(e) => {
                    errors.push(e);
                }
            }
        }
    }

    Err(crate::GvaClientError::Errors(errors))
}
