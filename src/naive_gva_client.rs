//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;

#[derive(Debug)]
pub struct NaiveGvaClient {
    gva_endpoint: reqwest::Url,
    inner: Client,
    raw_requestor: RawRequestor,
}

#[derive(Debug, Error)]
pub enum NaiveGvaClientBuildError {
    #[error("Invalid endpoint url: {0}")]
    InvalidUrl(String),
}

impl NaiveGvaClient {
    pub fn new(gva_endpoint: &str) -> Result<Self, NaiveGvaClientBuildError> {
        Ok(Self {
            gva_endpoint: reqwest::Url::parse(gva_endpoint)
                .map_err(|e| NaiveGvaClientBuildError::InvalidUrl(e.to_string()))?,
            inner: Client::new(),
            raw_requestor: raw_requestor(),
        })
    }
}

#[cfg(feature = "async")]
#[async_trait]
impl crate::gva_client::inner::GvaClientInner for NaiveGvaClient {
    #[inline(always)]
    async fn send_bca_query(&self, req: BcaReq) -> Result<BcaResp, GvaClientError> {
        self.raw_requestor
            .send_bca_query(req, &self.inner, self.gva_endpoint.clone())
            .await
    }
    async fn send_bca_batch_queries<S: Stream<Item = BcaReq> + Send + Sync + 'static>(
        &self,
        batch_stream: S,
    ) -> Result<Vec<BcaResult>, GvaClientError> {
        crate::raw_requestor::r#async::send_bca_batch_queries(
            batch_stream,
            self.inner.post(self.gva_endpoint.clone()),
        )
        .await
    }
}

#[cfg(feature = "blocking")]
impl crate::gva_client::inner::GvaClientInner for NaiveGvaClient {
    #[inline(always)]
    fn send_bca_query(&self, req: BcaReq) -> Result<BcaResp, GvaClientError> {
        self.raw_requestor
            .send_bca_query(req, &self.inner, self.gva_endpoint.clone())
    }
    fn send_bca_batch_queries<I: IntoIterator<Item = BcaReq> + Send + Sync>(
        &self,
        batch_iter: I,
    ) -> Result<Vec<BcaResult>, GvaClientError> {
        crate::raw_requestor::blocking::send_bca_batch_queries(
            batch_iter,
            self.inner.post(self.gva_endpoint.clone()),
        )
    }
}

impl GvaClient for NaiveGvaClient {}
