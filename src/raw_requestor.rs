//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;

#[cfg(feature = "async")]
pub(crate) mod r#async;
#[cfg(feature = "blocking")]
pub(crate) mod blocking;

use bincode::Options as _;
use duniter_bca_types::{bincode_opts, BcaReq};
#[cfg(feature = "blocking")]
use reqwest::blocking::RequestBuilder;
use reqwest::header::CONTENT_TYPE;
#[cfg(feature = "async")]
use reqwest::RequestBuilder;

#[derive(Clone, Debug)]
pub(crate) struct RawRequestor;

impl RawRequestor {
    #[allow(dead_code)]
    #[maybe_async]
    pub(crate) async fn send_bca_query(
        &self,
        bca_req: BcaReq,
        client: &Client,
        endpoint: Url,
    ) -> Result<BcaResp, GvaClientError> {
        send_bca_query(bca_req, client.post(endpoint)).await
    }
}

#[cfg(test)]
pub(crate) struct MockRawRequestor(
    Box<dyn Send + Sync + Fn(BcaReq, Url) -> Result<BcaResp, GvaClientError>>,
);

#[cfg(test)]
impl std::fmt::Debug for MockRawRequestor {
    fn fmt(&self, _: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        todo!()
    }
}

#[cfg(test)]
impl MockRawRequestor {
    pub(crate) fn new<
        F: 'static + Send + Sync + Fn(BcaReq, Url) -> Result<BcaResp, GvaClientError>,
    >(
        f: F,
    ) -> Self {
        Self(Box::new(f))
    }
    #[maybe_async]
    pub(crate) async fn send_bca_query(
        &self,
        bca_req: BcaReq,
        _: &Client,
        endpoint: Url,
    ) -> Result<BcaResp, GvaClientError> {
        self.0(bca_req, endpoint)
    }
}

#[maybe_async]
async fn send_bca_query(
    bca_req: BcaReq,
    request_builder: RequestBuilder,
) -> Result<BcaResp, GvaClientError> {
    let bca_req_bytes = serialize_bca_req(bca_req);
    let resp_bytes = request_builder
        .header(CONTENT_TYPE, "application/bincode")
        .body(bca_req_bytes)
        .send()
        .await
        .map_err(GvaClientError::Req)?
        .bytes()
        .await
        .map_err(GvaClientError::Req)?;

    let bca_res: Result<BcaResp, BcaReqExecError> = bincode_opts()
        .deserialize(&resp_bytes.slice(4..))
        .map_err(GvaClientError::FailToDeserResp)?;

    bca_res.map_err(GvaClientError::Bca)
}

fn serialize_bca_req(req: BcaReq) -> Vec<u8> {
    let mut buffer = Vec::with_capacity(64);
    buffer.push(0);
    buffer.push(0);
    buffer.push(0);
    buffer.push(0);
    bincode_opts()
        .serialize_into(&mut buffer, &req)
        .unwrap_or_else(|_| panic!("fail to ser req"));
    let req_size = buffer.len() - 4;
    buffer[..4].copy_from_slice(&u32::to_be_bytes(req_size as u32)[..]);
    buffer
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_serialize_bca_req() {
        let req = BcaReq::V0(BcaReqV0 {
            req_id: 42,
            req_type: BcaReqTypeV0::Ping,
        });

        let bytes = serialize_bca_req(req);
        assert_eq!(&bytes, &[0, 0, 0, 3, 0, 42, 6]);
    }
}
