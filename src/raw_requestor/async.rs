//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;
use async_io_stream::IoStream;
use futures::{Stream, StreamExt, TryStreamExt};
use reqwest::header::CONTENT_TYPE;
use std::convert::Infallible;

pub(crate) async fn send_bca_batch_queries(
    stream: impl Stream<Item = BcaReq> + Send + Sync + 'static,
    request_builder: reqwest::RequestBuilder,
) -> Result<Vec<Result<BcaResp, BcaReqExecError>>, GvaClientError> {
    let body = reqwest::Body::wrap_stream(
        stream.map(|bca_req| Ok::<_, Infallible>(super::serialize_bca_req(bca_req))),
    );

    match request_builder
        .header(CONTENT_TYPE, "application/bincode")
        .body(body)
        .send()
        .await
        .map_err(GvaClientError::Req)
    {
        Ok(response) => {
            let async_reader = IoStream::<_, bytes::Bytes>::new(
                response
                    .bytes_stream()
                    .map_err(|e| std::io::Error::new(std::io::ErrorKind::Other, e)),
            );
            async_bincode::AsyncBincodeReader::<_, Result<BcaResp, BcaReqExecError>>::from(
                async_reader,
            )
            .try_fold(
                Vec::new(),
                |mut vec: Vec<Result<BcaResp, BcaReqExecError>>, resp| {
                    vec.push(resp);
                    futures::future::ready(Ok(vec))
                },
            )
            .await
            .map_err(GvaClientError::FailToDeserResp)
        }
        Err(e) => Err(e),
    }
}

/*use std::error::Error;

#[derive(Debug)]
#[must_use = "readers do nothing unless polled"]
pub struct IntoAsyncRead<St, E>
where
    St: TryStream<Error = E> + Unpin,
    St::Ok: AsRef<[u8]>,
    E: 'static + Error + Send + Sync,
{
    stream: St,
    state: ReadState<St::Ok>,
}

impl<St, E> Unpin for IntoAsyncRead<St, E>
where
    St: TryStream<Error = E> + Unpin,
    St::Ok: AsRef<[u8]>,
    E: 'static + Error + Send + Sync,
{
}

#[derive(Debug)]
enum ReadState<T: AsRef<[u8]>> {
    Ready { chunk: T, chunk_start: usize },
    PendingChunk,
    Eof,
}

impl<St, E> IntoAsyncRead<St, E>
where
    St: TryStream<Error = E> + Unpin,
    St::Ok: AsRef<[u8]>,
    E: 'static + Error + Send + Sync,
{
    fn new(stream: St) -> Self {
        Self {
            stream,
            state: ReadState::PendingChunk,
        }
    }
}

use std::task::Poll;
impl<St, E> tokio::io::AsyncRead for IntoAsyncRead<St, E>
where
    St: TryStream<Error = E> + Unpin,
    St::Ok: AsRef<[u8]>,
    E: 'static + Error + Send + Sync,
{
    fn poll_read(
        mut self: std::pin::Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
        buf: &mut tokio::io::ReadBuf<'_>,
    ) -> std::task::Poll<std::io::Result<()>> {
        loop {
            match &mut self.state {
                ReadState::Ready { chunk, chunk_start } => {
                    let chunk = chunk.as_ref();
                    let len = std::cmp::min(buf.capacity(), chunk.len() - *chunk_start);

                    buf.put_slice(&chunk[*chunk_start..*chunk_start + len]);
                    *chunk_start += len;

                    if chunk.len() == *chunk_start {
                        self.state = ReadState::PendingChunk;
                    }

                    return Poll::Ready(Ok(()));
                }
                ReadState::PendingChunk => {
                    match futures::ready!(self.stream.try_poll_next_unpin(cx)) {
                        Some(Ok(chunk)) => {
                            if !chunk.as_ref().is_empty() {
                                self.state = ReadState::Ready {
                                    chunk,
                                    chunk_start: 0,
                                };
                            }
                        }
                        Some(Err(err)) => {
                            self.state = ReadState::Eof;
                            return Poll::Ready(Err(std::io::Error::new(
                                std::io::ErrorKind::Other,
                                err,
                            )));
                        }
                        None => {
                            self.state = ReadState::Eof;
                            return Poll::Ready(Ok(()));
                        }
                    }
                }
                ReadState::Eof => {
                    return Poll::Ready(Ok(()));
                }
            }
        }
    }
}*/
