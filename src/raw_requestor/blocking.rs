//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::*;
use bincode::Options;
use duniter_bca_types::bincode_opts;
use reqwest::header::CONTENT_TYPE;

pub(crate) fn send_bca_batch_queries<I: IntoIterator<Item = BcaReq>>(
    bca_batch_reqs: I,
    request_builder: reqwest::blocking::RequestBuilder,
) -> Result<Vec<BcaResult>, GvaClientError> {
    let bca_batch_reqs_bytes = serialize_bca_batch_reqs(bca_batch_reqs);
    let resp_bytes = request_builder
        .header(CONTENT_TYPE, "application/bincode")
        .body(bca_batch_reqs_bytes)
        .send()
        .map_err(GvaClientError::Req)?
        .bytes()
        .map_err(GvaClientError::Req)?;

    deserialize_bca_batch_resps(resp_bytes.as_ref()).map_err(GvaClientError::FailToDeserResp)
}

fn serialize_bca_batch_reqs<I: IntoIterator<Item = BcaReq>>(batch_reqs: I) -> Vec<u8> {
    let mut buffer = Vec::with_capacity(1_024);
    for req in batch_reqs {
        let len = buffer.len();
        buffer.push(0);
        buffer.push(0);
        buffer.push(0);
        buffer.push(0);
        bincode_opts()
            .serialize_into(&mut buffer, &req)
            .unwrap_or_else(|_| panic!("fail to ser req"));
        let req_size = buffer.len() - len - 4;
        buffer[len..len + 4].copy_from_slice(&u32::to_be_bytes(req_size as u32)[..]);
    }
    buffer
}

fn deserialize_bca_batch_resps(
    bytes: &[u8],
) -> Result<Vec<Result<BcaResp, BcaReqExecError>>, bincode::Error> {
    let mut vec = Vec::new();
    let end = bytes.len();
    let mut cursor = 0;
    let mut elem_size_array = [0u8; 4];
    loop {
        let remaining = end - cursor;
        if remaining == 0 {
            return Ok(vec);
        } else if remaining < 4 {
            return Err(bincode::ErrorKind::Io(std::io::Error::from(
                std::io::ErrorKind::BrokenPipe,
            ))
            .into());
        } else {
            elem_size_array.copy_from_slice(&bytes[cursor..cursor + 4]);
            let elem_size = u32::from_be_bytes(elem_size_array) as usize;
            cursor += 4;
            let elem: Result<BcaResp, BcaReqExecError> =
                bincode_opts().deserialize(&bytes[cursor..cursor + elem_size])?;
            cursor += elem_size;
            vec.push(elem);
        }
    }
}
#[cfg(test)]
mod tests {

    use super::*;
    use duniter_bca_types::{BcaReqTypeV0, BcaReqV0, BcaRespTypeV0, BcaRespV0};

    #[test]
    fn test_serialize_batch_reqs() {
        let req1 = BcaReq::V0(BcaReqV0 {
            req_id: 42,
            req_type: BcaReqTypeV0::Ping,
        });
        let req2 = BcaReq::V0(BcaReqV0 {
            req_id: 57,
            req_type: BcaReqTypeV0::MembersCount,
        });

        let bytes = serialize_bca_batch_reqs(vec![req1, req2].into_iter());
        assert_eq!(
            &bytes,
            &[
                0, 0, 0, 3, 0, 42, 6, // req1
                0, 0, 0, 3, 0, 57, 3, // req2
            ]
        );
    }

    #[test]
    fn test_deserialize_batch_resp() -> Result<(), bincode::Error> {
        assert_eq!(deserialize_bca_batch_resps(&[])?, vec![]);

        let bytes = &[
            0, 0, 0, 4, 0, 0, 42, 7, // resp1
            0, 0, 0, 5, 0, 0, 57, 5, 73, // resp2
        ];

        assert_eq!(
            deserialize_bca_batch_resps(bytes)?,
            vec![
                Ok(BcaResp::V0(BcaRespV0 {
                    req_id: 42,
                    resp_type: BcaRespTypeV0::Pong,
                })),
                Ok(BcaResp::V0(BcaRespV0 {
                    req_id: 57,
                    resp_type: BcaRespTypeV0::MembersCount(73),
                }))
            ]
        );

        Ok(())
    }
}
