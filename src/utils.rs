//  Copyright (C) 2020 Éloïs SANCHEZ.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use rand::prelude::*;

#[cfg(not(test))]
pub(crate) fn choose_multiple<T: ?Sized + SliceRandom>(
    t: &T,
    amount: usize,
) -> rand::seq::SliceChooseIter<T, T::Item> {
    t.choose_multiple(&mut rand::thread_rng(), amount)
}
#[cfg(test)]
pub(crate) fn choose_multiple<T: ?Sized + SliceRandom>(
    t: &T,
    amount: usize,
) -> rand::seq::SliceChooseIter<T, T::Item> {
    t.choose_multiple(&mut rand_chacha::ChaCha8Rng::from_seed([42u8; 32]), amount)
}

/*#[cfg(not(test))]
fn partial_shuffle<T>(slice: &mut [T], amount: usize) -> (&mut [T], &mut [T]) {
    slice.partial_shuffle(&mut rand::thread_rng(), amount)
}
#[cfg(test)]
fn partial_shuffle<T>(slice: &mut [T], amount: usize) -> (&mut [T], &mut [T]) {
    slice.partial_shuffle(&mut rand_chacha::ChaCha8Rng::from_seed([42u8; 32]), amount)
}*/
